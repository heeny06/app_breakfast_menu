import 'package:app_breakfast_menu/model/food.dart';

final List<Food> configFoodMookup = [
  Food('assets/2.jpg', '샌드위치', '탄수화물', '400kal'),
  Food('assets/3.jpg', '샐러드', '식이섬유', '390kal'),
  Food('assets/4.jpg', '요거트', '유지방', '320kal'),
];

final List<Food> configFoodSpecialList = [
  Food('assets/5.jpg', '그래놀라', '탄수화물', '420kal'),
  Food('assets/6.jpg', '베이글', '탄수화물', '450kal'),
  Food('assets/7.jpg', '과일', '식이섬유', '390kal'),
];