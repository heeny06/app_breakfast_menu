import 'package:flutter/material.dart';

class FoodItem extends StatelessWidget {
  FoodItem({super.key, required this.photoUrl, required this.foodName, required this.foodIngredient, required this.foodCalorie, this.imgWidth = 100, this.imgHeight = 100, required this.callback});

  final String photoUrl;
  final String foodName;
  final String foodIngredient;
  final String foodCalorie;
  double imgWidth;
  double imgHeight;
  final VoidCallback callback;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: Container(
        child: Column(
          children: [
            Image.asset(photoUrl, width: imgWidth, height: imgHeight,),
            Container(
              child: Text(foodName),
            ),
            Container(
              child: Text(foodIngredient),
            ),
            Container(
              child: Text(foodCalorie),
            ),
          ],
        ),
      ),
      onTap: callback,
    );
  }
}
