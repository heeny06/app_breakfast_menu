import 'package:flutter/material.dart';

class FoodInfoBox extends StatelessWidget {
  const FoodInfoBox({super.key, required this.title, required this.contents});

  final String title;
  final String contents;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          Text(title),
          Divider(),
          Text(contents),
        ],
      ),
    );
  }
}
