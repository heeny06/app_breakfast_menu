import 'package:app_breakfast_menu/components/food_item.dart';
import 'package:app_breakfast_menu/components/main_title.dart';
import 'package:app_breakfast_menu/config/config_food_data.dart';
import 'package:app_breakfast_menu/pages/page_food_detail.dart';
import 'package:flutter/material.dart';

class PageIndex extends StatefulWidget {
  const PageIndex({Key? key}) : super(key: key);

  @override
  State<PageIndex> createState() => _PageIndexState();
}

class _PageIndexState extends State<PageIndex> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Breakfast Menu'),
      ),
      body: SingleChildScrollView(
        child: Container(
          child: Column(
            children: [
              Image.asset('assets/1.jpg'),
              const SizedBox(
                height: 50,
              ),
              const MainTitle(
                title: 'New',
              ),
              SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: Row(
                  children: [
                    for (int i = 0; i < configFoodMookup.length; i++)
                      FoodItem(
                        photoUrl: configFoodMookup[i].photoUrl,
                        foodName: configFoodMookup[i].foodName,
                        foodIngredient: configFoodMookup[i].foodIngredient,
                        foodCalorie: configFoodMookup[i].foodCalorie,
                        imgWidth: 150,
                        imgHeight: 150,
                        callback: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => PageFoodDetail(food: configFoodMookup[i],)));
                        },
                      ),
                  ],
                ),
              ),
              const SizedBox(
                height: 70,
              ),
              const MainTitle(title: 'Best'),
              SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: Row(
                  children: [
                    for (int i = 0; i < configFoodSpecialList.length; i++)
                      FoodItem(
                          photoUrl: configFoodSpecialList[i].photoUrl,
                          foodName: configFoodSpecialList[i].foodName,
                          foodIngredient: configFoodSpecialList[i].foodIngredient,
                          foodCalorie: configFoodSpecialList[i].foodCalorie,
                          callback: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => PageFoodDetail(food: configFoodSpecialList[i],)));
                          }),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
