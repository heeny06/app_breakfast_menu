import 'package:app_breakfast_menu/components/food_info_box.dart';
import 'package:app_breakfast_menu/model/food.dart';
import 'package:flutter/material.dart';

class PageFoodDetail extends StatefulWidget {
  const PageFoodDetail({super.key, required this.food});

  final Food food;

  @override
  State<PageFoodDetail> createState() => _PageFoodDetailState();
}

class _PageFoodDetailState extends State<PageFoodDetail> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Detail'),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Image.asset(widget.food.photoUrl),
            Container(
              child: Column(
                children: [
                  Text(widget.food.foodName),
                  Divider(),
                  FoodInfoBox(title: '영양성분', contents: widget.food.foodIngredient),
                  Divider(),
                  FoodInfoBox(title: '칼로리', contents: widget.food.foodCalorie),
                ],
              ),
            )
          ],
        ),
      ),
      bottomNavigationBar: BottomAppBar(
        shape: CircularNotchedRectangle(),
        child: OutlinedButton(
          child: const Text('구매하기'),
          onPressed: () {},
        ),
      ),
    );
  }
}
