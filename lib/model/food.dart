class Food {
  String photoUrl;
  String foodName;
  String foodIngredient;
  String foodCalorie;

  Food(this.photoUrl, this.foodName, this.foodIngredient, this.foodCalorie);
}